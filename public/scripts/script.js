import game from './businessObjects/game.js';

const mainGridElement = document.getElementById('mainGrid');
const startAreaElement = document.getElementById('startArea');
const nameInputElement = document.getElementById('nameInput');
const startGameButtonElement = document.getElementById('startGameButton');
const gameCommandsAreaElement = document.getElementById('gameCommandsArea');
const hitButtonElement = document.getElementById('hitButton');
const standButtonElement = document.getElementById('standButton');
const dealerCardsElement = document.getElementById('dealerCards');
const userNameElement = document.getElementById('userName');
const userCardsElement = document.getElementById('userCards');
const playerPointsElement = document.getElementById('playerPoints');
const gameOverModalElement = document.getElementById('gameOverModal');
const gameOverMessageElement = document.getElementById('gameOverMessage');
const okModalButtonElement = document.getElementById('okModalButton');

(function init() {
    nameInputElement.value = 'Player';
    startAreaElement.style.display = 'block';
    gameCommandsAreaElement.style.display = 'none';
})()

const toggleButtonsEnabled = (enabled) => {
    startGameButtonElement.disabled = !enabled;
    hitButtonElement.disabled = !enabled;
    standButtonElement.disabled = !enabled;
}

const handleStartGameClick = () => {
    startAreaElement.style.display = 'none';
    gameCommandsAreaElement.style.display = 'block';
    userNameElement.innerText = nameInputElement.value;
    toggleButtonsEnabled(false);
    game.startGame().then(p => {
            showCards(game.getDealerCards(), dealerCardsElement, true)
            showCards(game.getUserCards(), userCardsElement);
            showPoints();
            if (game.isGameOver()) {
                showCards(game.getDealerCards(), dealerCardsElement);
                handleGameOver();
            } else {
                toggleButtonsEnabled(true);
            }
        });
}

const showCards = (cards, cardsElement, hideSecondCard = false) => {
    cardsElement.innerText = '';
    for (let i = 0; i < cards.length; i++) {
        const image = document.createElement('img');
        image.src = cards[i].image;
        image.alt = cards[i].code;
        image.height = '150';
        
        if (i === 1 && hideSecondCard) {
            image.src = './assets/images/back.png';
            image.alt = 'Hidden dealer card';
        }

        cardsElement.appendChild(image);
    };
}

const showPoints = () => {
    playerPointsElement.innerText = 'Current points: ' + game.getUserPoints();
}

const handleHitClick = () => {
    toggleButtonsEnabled(false);
    game.hitUser().then(p => {
            showCards(game.getUserCards(), userCardsElement);
            showPoints();
            if (game.isGameOver()) {
                showCards(game.getDealerCards(), dealerCardsElement);
                handleGameOver();
            } else {
                toggleButtonsEnabled(true);
            }
        });
}

const handleStandClick = () => {
    toggleButtonsEnabled(false);
    let winner = null;
    game.stand()
        .then(p => {
            showCards(game.getDealerCards(), dealerCardsElement);
            handleGameOver();
        });
}

const handleGameOver= () => {
    startAreaElement.style.display = 'block';
    gameCommandsAreaElement.style.display = 'none';
    gameOverMessageElement.innerText = game.whoWon();
    showModal(gameOverModalElement);
}

const handleGameOverOkModalButtonClick = () => {
    hideModal(gameOverModalElement);
}

const showModal = (modal) => {
    modal.style.display = 'block';
    mainGridElement.style.opacity = '35%';
    toggleButtonsEnabled(false);
}

const hideModal = (modal) => {
    modal.style.display = 'none';
    mainGridElement.style.opacity = '100%';
    toggleButtonsEnabled(true);
}

startGameButtonElement.addEventListener('click', handleStartGameClick);
hitButtonElement.addEventListener('click', handleHitClick);
standButtonElement.addEventListener('click', handleStandClick);
okModalButtonElement.addEventListener('click', handleGameOverOkModalButtonClick);
