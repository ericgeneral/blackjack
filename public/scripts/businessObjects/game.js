import Player from './player.js';

const game = (function() {
    let deck = null;
    let dealer = null;
    let user = null;
    let gameOver = true;
    let apiError = false;

    const deckOfCardsBaseUrl = 'https://deckofcardsapi.com/';

    const shuffleCards = async() => {
        try {
            if (deck === null) {
                const response = await fetch(deckOfCardsBaseUrl + 'api/deck/new/shuffle/?deck_count=6');
                deck = await response.json();
                return deck;
            } else {
                const response = await fetch(deckOfCardsBaseUrl + 'api/deck/' + deck.deck_id + '/shuffle/');
                deck = await response.json();
                return deck;
            }
        } catch(error) {
            console.log(error);
            gameOver = true;
            apiError = true;
        }
    }

    const dealNewGame = async() => {
        try {
            const response = await fetch(deckOfCardsBaseUrl + 'api/deck/' + deck.deck_id + '/draw/?count=4');
            const parsedResponse = await response.json();
            user.cards.push(parsedResponse.cards[0]);
            dealer.cards.push(parsedResponse.cards[1]);
            user.cards.push(parsedResponse.cards[2]);
            dealer.cards.push(parsedResponse.cards[3]);
            return [parsedResponse];
        } catch(error) {
            console.log(error);
            gameOver = true;
            apiError = true;
        }
    }

    const dealCard = async(player) => {
        try {
            const response = await fetch(deckOfCardsBaseUrl + 'api/deck/' + deck.deck_id + '/draw/?count=1');
            const parsedResponse = await response.json();
            player.cards.push(parsedResponse.cards[0]);
            return parsedResponse;
        } catch(error) {
            console.log(error);
            gameOver = true;
            apiError = true;
        }
    }

    const calculatePoints = async(player) => {
        player.points = 0;
        player.cards.forEach(card => {
            if (card.value === 'KING' || card.value === 'QUEEN' || card.value === 'JACK') {
                player.points += 10;
            } else if (card.value === 'ACE') {
                player.points += 11;
            } else {
                player.points += parseInt(card.value);
            }
        })
        if (player.points > 21) {
            handleAces(player);
        }
        if (player.points > 21) {
            gameOver = true;
        }
        return player.points;
    }

    const handleAces = (player) => {
        player.cards.forEach(card => {
            if (card.value === 'ACE') {
                card.value = '1';
                player.points -= 10;
            }
            if (player.points <= 21) {
                return;
            }
        })
    }

    const dealerPlays = async() => {
        if (dealer.points >= 17) {
            return dealer.points;
        }
        var result = dealCard(dealer)
            .then(p => calculatePoints(dealer))
            .then(p => dealerPlays());
        return result;
    }

    return {
        startGame: async function() {
            gameOver = false;
            apiError = false;
            dealer = new Player();
            user = new Player();
            var result = shuffleCards()
                .then(p => dealNewGame())
                .then(p => calculatePoints(dealer))
                .then(p => calculatePoints(user));
            return result;
        },
        getDealerCards: function() {
            return dealer.cards;
        },
        getUserCards: function() {
            return user.cards;
        },
        getDealerPoints: function() {
            return dealer.points;
        },
        getUserPoints: function() {
            return user.points;
        },
        hitUser: async function() {
            var result = dealCard(user).then(p => calculatePoints(user));
            return result;
        },
        stand: async function() {
            var resp = dealerPlays();
            gameOver = true;
            return resp;
        },
        isGameOver() {
            return gameOver;
        },
        whoWon: function() {
            if (apiError) {
                return "There was an error with the deckofcardsapi.";
            }
            if (user.points > 21 && dealer.points > 21) {
                return `Draw! You busted with ${user.points} points, and the dealer busted with ${dealer.points} points.`;
            }
            if (user.points > 21) {
                return `Dealer wins! You busted with ${user.points} points, and the dealer had ${dealer.points} points.`;
            }
            if (dealer.points > 21) {
                return `You win! You had ${user.points} points, and the dealer busted with ${dealer.points} points.`;
            }
            if (user.points > dealer.points) {
                return `You win! You had ${user.points} points, and the dealer had ${dealer.points} points.`;
            }
            if (user.points < dealer.points) {
                return `Dealer wins! You had ${user.points} points, and the dealer had ${dealer.points} points.`;
            }
            return `Draw! Both you and the dealer had ${user.points} points.`;
        }
    }

})()

export default game;